﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models
{
    public class Admin
    {
        [Required]
        public int Id { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no names that long")]
        public string UserName { get; set; }
      } 
}
