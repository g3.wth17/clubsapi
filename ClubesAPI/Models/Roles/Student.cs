﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models
{
    public class Student
    {
        [Required]
        public int Id { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no names that long")]
        public string Name { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no lastnames that long")]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public GenreType Genre { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no School that long")]
        public string School { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no User Name that long")]
        public string UserName { get; set; }

    }


}
