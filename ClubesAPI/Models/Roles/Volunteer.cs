﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models
{
    public class Volunteer
    {
        [Required]
        public int Id { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no names that long")]
        public string Name { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no lastnames that long")]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public GenreType Genre { get; set; }
        [Required(ErrorMessage ="For security reasons we need your number")]
        public int CellphoneNumber { get; set; }
        [Required(ErrorMessage ="To get you in contact we need this field"), StringLength(30, ErrorMessage ="No email address is that long")]
        public string EmailAddress { get; set; }
        public Club CreatedClub { get; set; }
    }


}
