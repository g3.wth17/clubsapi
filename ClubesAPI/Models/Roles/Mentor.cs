﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models
{
    public class Mentor
    {
        [Required]
        public int Id { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no names that long")]
        public string Name { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no lastnames that long")]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public GenreType Genre { get; set; }
        [Required, StringLength(20, ErrorMessage = "There are no Country that long")]
        public string Country { get; set; }
        [Required]
        public int PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
