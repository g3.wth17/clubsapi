﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models
{
    public class Club
    {
        public int Id { get; set; }
        public string ClubName { get; set; }
        public string SchoolName { get; set; }
        public int MaximumMembers { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int AttendantId { get; set; }
    }
}
