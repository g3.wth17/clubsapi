﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models.Facilitator_Tools
{
    public class Course
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        
        //FOREING KEY//
        [Required]
        public int IdFacilitator { get; set; }
    }
}
