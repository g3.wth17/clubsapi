﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Models.Facilitator_Tools
{
    public class Module
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int IdCourse { get; set; }
        [Required]
        public int IdFacilitator { get; set; }
        public string TitleModule { get; set; }
        public string Content { get; set; }
    }
}
