﻿using ClubesAPI.Models;
using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Data.Repository
{
    public interface IInnovationClubsRepository
    {

        //Adviser's Methods
        IEnumerable<Adviser> GetAdvisers();
        Adviser GetAdviser(int id);
        Adviser CreateAdviser(Adviser adviser);

        //Facilitator's Methods
        IEnumerable<Facilitator> GetFacilitators();
        Facilitator GetFacilitator(int id);
        Facilitator CreateFacilitator(Facilitator facilitator);

        //Volunteer's Methods
        IEnumerable<Volunteer> GetVolunteers();
        Volunteer GetVolunteer(int id);
        Volunteer CreateVolunteer(Volunteer volunteer);

  	     //STUDENT
        Student CreateStudent(Student student);
        Student GetStudent(int id);
        IEnumerable<Student> GetStudents();

        //ADMIN
        Admin CreateAdmin(Admin admin);
        Admin GetAdmin(int id);
        IEnumerable<Admin> GetAdmins();


        //MENTOR
        Mentor CreateMentor(Mentor mentor);
        Mentor GetMentor(int id);
        IEnumerable<Mentor> GetMentors();

        IEnumerable<Club> GetClubs();
        Club GetClub(int id, int ownerId);
        Club GetClub(int id);
        Club CreateClub(Club club);
        

        //COURSE
        Course CreateCourse(Course course);
        IEnumerable<Course> GetCourses(int facilitatorId);
        Course GetCourse(int facilitatorId, int id);

        //MODULE

        Module CreateModule(Module module);
        IEnumerable<Module> GetModules(int facilitatorId, int courseId);
        Module GetModule(int id, int facilitatorId, int courseId);
    }
}
