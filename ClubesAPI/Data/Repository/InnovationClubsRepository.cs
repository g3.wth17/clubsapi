﻿using ClubesAPI.Models;
using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Data.Repository 
{
    public class InnovationClubsRepository : IInnovationClubsRepository
    {
        //roles
        List<Adviser> advisers;
        List<Facilitator> facilitators;
        List<Volunteer> volunteers;
        List<Student> students;
        List<Admin> admins;
        List<Mentor> mentors;

        List<Club> clubs;
        //Facilitator-tools
        List<Course> courses;
        List<Module> modules;
        public InnovationClubsRepository()
        {
         
            advisers = new List<Adviser>();
            advisers.Add(new Adviser(){                
                    Id = 1,
                    Name = "Pedro",
                    LastName = "Pedrazas",
                    BirthDate = new DateTime(2020, 3, 22),
                    Subject = "Math",
                    CellphoneNumber = 12334567,
                    Genre = GenreType.Male,
                    EmailAddress = "something@domain.com"
            }) ;
            advisers.Add(new Adviser()
            {
                Id = 2,
                Name = "Ana",
                LastName = "Pedrazas",
                BirthDate = new DateTime(2015, 3, 22),
                Subject = "Math",
                CellphoneNumber = 12334567,
                Genre = GenreType.Female,
                EmailAddress = "something@domain.com"
            });

            facilitators = new List<Facilitator>();
            facilitators.Add(new Facilitator()
            {
                Id = 1,
                Name = "Lula",
                LastName = "Pedrazas",
                BirthDate = new DateTime(2015, 3, 22),
                CellphoneNumber = 12334567,
                Genre = GenreType.Female,
                EmailAddress = "something@domain.com",
                Country = "Spain",
                Speciality = "Robotics" 
            }) ;
            facilitators.Add(new Facilitator()
            {
                Id = 2,
                Name = "Mariana",
                LastName = "Dominguez",
                BirthDate = new DateTime(2015, 3, 22),
                CellphoneNumber = 3322,
                Genre = GenreType.Female,
                EmailAddress = "something@domain.com",
                Country = "Germany",
                Speciality = "Others"
            });

            volunteers = new List<Volunteer>();
            volunteers.Add(new Volunteer()
            {
                Id = 1,
                Name = "Tito",
                LastName = "Pedrazas",
                BirthDate = new DateTime(2015, 3, 22),
                CellphoneNumber = 12334567,
                Genre = GenreType.Female,
                EmailAddress = "something@domain.com"
            });


            //Students
            students = new List<Student>();
            students.Add(new Student() { Id = 1,
                Name = "Pedro", 
                LastName = "Pedrazas", 
                BirthDate = new DateTime(2020, 2, 22),
                Genre = GenreType.Male, 
                School = "School1",
                UserName = "Juan666" });
            students.Add(new Student() { 
                Id = 2, Name = "Rosa", 
                LastName = "Rosales",
                BirthDate = new DateTime(2019, 2, 22),
                Genre = GenreType.Female,
                School = "School2", 
                UserName = "Dira_12" });

            //Admins
            admins = new List<Admin>();
            admins.Add(new Admin() { 
                Id = 1,
                UserName = "Alejandro" });

            mentors = new List<Mentor>();
            mentors.Add(new Mentor() {
                Id = 1, 
                Name = "Mariana",
                LastName = "Normalin",
                BirthDate = new DateTime(2019, 2, 22),
                Genre = GenreType.Female, 
                Country = "Spain", 
                Email = "dqwd@gmail.com", 
                PhoneNumber = 77451352 });

            clubs = new List<Club>();


            //Courses

            courses = new List<Course>();
            courses.Add(new Course()
            {
                Id = 1,
                IdFacilitator = 1, 
                Title="Title 1",
                Description="No Description"
            });
            courses.Add(new Course()
            {
                Id = 2,
                IdFacilitator = 2,
                Title = "Title 2",
                Description = "No Description pp"
            });
            courses.Add(new Course()
            {
                Id = 3,
                IdFacilitator = 1,
                Title = "Title 3",
                Description = "No Description ee"
            });


            modules = new List<Module>();
            modules.Add(new Module()
            {
                Id = 1,
                IdCourse =1,
                IdFacilitator =1,
                TitleModule= "TITLE MODULE 1",
                Content= "TEXT 1"
            });
            modules.Add(new Module()
            {
                Id = 2,
                IdCourse = 1,
                IdFacilitator = 1,
                TitleModule = "TITLE MODULE 2",
                Content = "TEXT 2"
            });
            modules.Add(new Module()
            {
                Id = 3,
                IdCourse = 1,
                IdFacilitator = 1,
                TitleModule = "TITLE MODULE 3",
                Content = "TEXT 3"
            });
            modules.Add(new Module()
            {
                Id = 4,
                IdCourse = 3,
                IdFacilitator = 1,
                TitleModule = "TITLE MODULE COURSE 3 1",
                Content = "TEXT 1"
            });
            modules.Add(new Module()
            {
                Id = 5,
                IdCourse = 3,
                IdFacilitator = 1,
                TitleModule = "TITLE MODULE COURSE 3 2",
                Content = "TEXT 1"
            });
            modules.Add(new Module()
            {
                Id = 6,
                IdCourse = 2,
                IdFacilitator = 2,
                TitleModule = "TITLE MODULE FACILITATOR 2 1",
                Content = "TEXT 1 FACILITATOR 2"
            });
            modules.Add(new Module()
            {
                Id = 7,
                IdCourse = 2,
                IdFacilitator = 2,
                TitleModule = "TITLE MODULE FACILITATOR 2 1",
                Content = "TEXT 1 FACILITATOR 2"
            });
            modules.Add(new Module()
            {
                Id = 8,
                IdCourse = 2,
                IdFacilitator = 2,
                TitleModule = "TITLE MODULE FACILITATOR 2 1",
                Content = "TEXT 1 FACILITATOR 2"
            });

        }


        //ADVISER//
        public Adviser CreateAdviser(Adviser adviser)
        {
            var lastId = advisers.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            adviser.Id = nextId;
            advisers.Add(adviser);
            return adviser;
        }

        public Adviser GetAdviser(int id)
        {
            return advisers.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Adviser> GetAdvisers()
        {
            return advisers;
        }

        //FACILITATOR//

        public Facilitator CreateFacilitator(Facilitator facilitator)
        {
            var lastId = facilitators.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            facilitator.Id = nextId;
            facilitators.Add(facilitator);
            return facilitator;
        }
        public Facilitator GetFacilitator(int id)
        {
            return facilitators.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Facilitator> GetFacilitators()
        {
            return facilitators;
        }
        //VOLUNTEER//
        public Volunteer CreateVolunteer(Volunteer volunteer)
        {
            var lastId = volunteers.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1; //ratas
            volunteer.Id = nextId;
            volunteers.Add(volunteer);
            return volunteer;
        }

        public Volunteer GetVolunteer(int id)
        {
            return volunteers.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Volunteer> GetVolunteers()
        {
            return volunteers;
        }


        // -------------- STUDENT ------------- \\
        public Student CreateStudent(Student student)
        {
            var lastId = students.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            student.Id = nextId;
            students.Add(student);
            return student;
        }

        public Student GetStudent(int id)
        {
            return students.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Student> GetStudents()
        {
            return students;
        }
        // -------------- ADMIN ------------- \\

        public Admin CreateAdmin(Admin admin)
        {
            var lastId = admins.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            admin.Id = nextId;
            admins.Add(admin);
            return admin;
        }

        public Admin GetAdmin(int id)
        {
            return admins.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Admin> GetAdmins()
        {
            return admins;
        }

        // -------------- MENTOR ------------- \\
        public Mentor CreateMentor(Mentor mentor)
        {
            var lastId = mentors.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            mentor.Id = nextId;
            mentors.Add(mentor);
            return mentor;
        }

        public Mentor GetMentor(int id)
        {
            return mentors.SingleOrDefault(u => u.Id == id);
        }

        public IEnumerable<Mentor> GetMentors()
        {
            return mentors;
        }

        public IEnumerable<Club> GetClubs()
        {
            return clubs;
        }

        public Club GetClub(int id, int ownerId)
        {
            return clubs.SingleOrDefault(c => c.Id == id && c.AttendantId == ownerId);
        }

        public Club CreateClub(Club club)
        {
            var lastId = clubs.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            club.Id = nextId;
            clubs.Add(club);
            return club;
        }

        public Club GetClub(int id)
        {
            return clubs.SingleOrDefault(c => c.Id == id);
        }
        // -------------- Courses ------------------\\
        public Course CreateCourse(Course course)
        {
            var lastId = courses.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            course.Id = nextId;
            courses.Add(course);
            return course;
        }

        public IEnumerable<Course> GetCourses(int facilitatorId)
        {
            return courses.Where(x => x.IdFacilitator == facilitatorId);
        }

        public Course GetCourse(int facilitatorId, int id)
        {
            return courses.Where(x => x.IdFacilitator == facilitatorId).SingleOrDefault(x =>x.Id == id);
        }

        // ---------------------- Module -------------\\ 
        public Module GetModule(int id, int courseId, int facilitatorId)
        {
            return modules.SingleOrDefault(x => x.Id == id && x.IdFacilitator == facilitatorId && x.IdCourse == courseId);
        }

        public Module CreateModule(Module module)
        {
            var lastId = modules.OrderByDescending(u => u.Id).FirstOrDefault();
            var nextId = lastId == null ? 1 : lastId.Id + 1;
            module.Id = nextId;
            modules.Add(module);
            return module;
        }

        public IEnumerable<Module> GetModules(int facilitatorId, int courseId)
        {
            return modules.Where(x => x.IdFacilitator == facilitatorId && x.IdCourse == courseId);

        }
    }
}
