﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubsController : ControllerBase
    {
        private IClubService clubController;
        public ClubsController(IClubService clubController)
        {
            this.clubController = clubController;
        }
        // GET: api/Clubs
        [HttpGet]
        public ActionResult<IEnumerable<Club>> Get()
        {
            try
            {
                return Ok(clubController.GetClubs());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET: api/Clubs/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Club> Get(int id)
        {
            try
            {
                var user = clubController.GetClub(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
