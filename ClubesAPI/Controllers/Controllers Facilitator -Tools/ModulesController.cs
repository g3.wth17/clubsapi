﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models.Facilitator_Tools;
using ClubesAPI.Services.Services_Facilitator_Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/facilitators/{IdFacilitator:int}/courses/{IdCourse:int}/modules")]
    [ApiController]
    public class ModulesController : ControllerBase
    {
        private readonly IModuleService moduleService;

        public ModulesController(IModuleService moduleService)
        {
            this.moduleService = moduleService;
        }
        // GET: api/Modules
        [HttpGet]
        public ActionResult<IEnumerable<Module>> Get(int IdFacilitator,int IdCourse)
        {
            try
            {
                return Ok(moduleService.GetModules(IdFacilitator, IdCourse));
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Module> Get(int IdFacilitator,int IdCourse, int id)
        {
            try
            {
                var course = moduleService.GetModule(IdFacilitator, IdCourse, id);
                return Ok(course);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        // POST api/values
        [HttpPost]
        public ActionResult<Module> Post(int IdFacilitator,int IdCourse, [FromBody] Module module)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdModule = moduleService.AddModule(IdFacilitator, IdCourse, module);
                return Created($"api/facilitators/{IdFacilitator}/courses/{IdCourse}/modules/{module.Id}", createdModule);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }

        }
    }
}
