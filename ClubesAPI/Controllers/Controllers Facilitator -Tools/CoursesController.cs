﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models.Facilitator_Tools;
using ClubesAPI.Services.Services_Facilitator_Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/facilitators/{IdFacilitator:int}/courses")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseService courseService;

        public CoursesController(ICourseService courseService)
        {
            this.courseService = courseService;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Course>> Get(int IdFacilitator)
        {
            try
            {
                return Ok(courseService.GetCourses(IdFacilitator));
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Course> Get(int IdFacilitator, int id)
        {
            try
            {
                var course = courseService.GetCourse(IdFacilitator,id);
                return Ok(course);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        // POST api/values
        [HttpPost]
        public ActionResult<Course> Post(int IdFacilitator, [FromBody] Course course)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdCourse = courseService.AddCourse(IdFacilitator, course);
                return Created($"api/facilitators/{IdFacilitator}/courses/{createdCourse.Id}", createdCourse);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }

        }
    }
}
