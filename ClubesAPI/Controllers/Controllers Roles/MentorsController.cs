﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MentorsController : ControllerBase
    {
        private IMentorService mentorService;

        public MentorsController(IMentorService mentorService)
        {
            this.mentorService = mentorService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Mentor>> Get()
        {
            try
            {
                return Ok(mentorService.GetMentors());
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        public ActionResult<Mentor> Get(int id)
        {
            try
            {
                var admin = mentorService.GetMentor(id);
                return Ok(admin);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
        [HttpPost]
        public ActionResult<Mentor> Post([FromBody] Mentor student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdAdmin = mentorService.AddMentor(student);
                return Created($"/api/Student/{createdAdmin.Id}", createdAdmin);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

    }
}
