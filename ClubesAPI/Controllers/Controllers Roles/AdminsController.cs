﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminsController : ControllerBase
    {
        private IAdminService adminService;

        public AdminsController(IAdminService adminService)
        {
            this.adminService = adminService;
        }
        // GET: api/values
        [HttpGet]
        public ActionResult<IEnumerable<Admin>> Get()
        {
            try
            {
                return Ok(adminService.GetAdmins());
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET: api/values/5
        [HttpGet("{id}")]
        public ActionResult<Admin> Get(int id)
        {
            try
            {
                var admin = adminService.GetAdmin(id);
                return Ok(admin);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST: api/values
        [HttpPost]
        public ActionResult<Admin> Post([FromBody] Admin admin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdAdmin = adminService.AddAdmin(admin);
                return Created($"/api/Student/{createdAdmin.Id}", createdAdmin);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }
    }
}
