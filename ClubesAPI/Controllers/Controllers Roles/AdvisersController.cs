﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [ApiController]
    public class AdvisersController : ControllerBase
    {
        private IAdviserService adviserService;
        private IClubService clubService;

        public AdvisersController(IAdviserService adviserService, IClubService clubService)
        {
            this.adviserService = adviserService;
            this.clubService = clubService;
        }

        // GET api/values

        [Route("api/[controller]")]
        [HttpGet]
        public ActionResult<IEnumerable<Adviser>> Get()
        {
            try
            {
                return Ok(adviserService.GetAdvisers());
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET api/values/5

        [Route("api/[controller]")]
        [HttpGet("{id}")]
        public ActionResult<Adviser> Get(int id)
        {
            try
            {
                var user = adviserService.GetAdviser(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/values

        [Route("api/[controller]")]
        [HttpPost]
        public ActionResult<Adviser> Post([FromBody] Adviser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdUser = adviserService.AddAdviser(user);
                return Created($"/api/advisers/{createdUser.Id}", createdUser);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }
        [Route("api/[controller]/{ownerId:int}/club/{id:int}")]
        [HttpGet]
        public ActionResult<Club> GetClub(int ownerId, int id)
        {
            try
            {
                var club = clubService.GetClub(id, ownerId);
                return Ok(club);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [Route("api/[controller]/{ownerId:int}/club")]
        [HttpPost]
        public ActionResult<Club> PostClub([FromBody] Club club, int ownerId)
        {
            try
            {
                var createdClub = clubService.AddClub(club, ownerId);
                return Created($"/api/volunteers/{ownerId}/clubs/{createdClub.Id}", createdClub);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    

    }
}