﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [ApiController]
    public class VolunteersController : ControllerBase
    {
        private IVolunteerService volunteerService;
        private IClubService clubService;
        public VolunteersController(IVolunteerService volunteerService, IClubService clubService)
        {
            this.volunteerService = volunteerService;
            this.clubService = clubService;
        }
        // GET api/values
        [Route("api/[controller]")]
        [HttpGet]
        public ActionResult<IEnumerable<Volunteer>> Get()
        {
            try
            {
                return Ok(volunteerService.GetVolunteers());
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET api/values/5
        [Route("api/[controller]")]
        [HttpGet("{id}")]
        public ActionResult<Volunteer> Get(int id)
        {
            try
            {
                var user = volunteerService.GetVolunteer(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/values
        [Route("api/[controller]")]
        [HttpPost]
        public ActionResult<Volunteer> Post([FromBody] Volunteer user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdUser = volunteerService.AddVolunteer(user);
                return Created($"/api/volunteers/{createdUser.Id}", createdUser);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }

        }
        [Route("api/[controller]/{ownerId:int}/club/{id:int}")]
        [HttpGet]
        public ActionResult<Club> GetClub(int ownerId, int id)
        {
            try
            {
                var club = clubService.GetClub(id, ownerId);
                return Ok(club);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [Route("api/[controller]/{ownerId:int}/club")]
        [HttpPost]
        public ActionResult<Club> PostClub([FromBody] Club club, int ownerId)
        {
            try
            {
                var createdClub = clubService.AddClub(club, ownerId);
                return Created($"/api/volunteers/{ownerId}/clubs/{createdClub.Id}", createdClub);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

    }
}