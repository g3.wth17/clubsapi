﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClubesAPI.Models;
using ClubesAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClubesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacilitatorsController : ControllerBase
    {
        private IFacilitatorService facilitadorService;
        public FacilitatorsController(IFacilitatorService facilitadorService)
        {
            this.facilitadorService = facilitadorService;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Facilitator>> Get()
        {
            try
            {
                return Ok(facilitadorService.GetFacilitators());
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Facilitator> Get(int id)
        {
            try
            {
                var user = facilitadorService.GetFacilitator(id);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Facilitator> Post([FromBody] Facilitator user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var createdUser = facilitadorService.AddFacilitator(user);
                return Created($"/api/Facilitators/{createdUser.Id}", createdUser);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Something bad happened: {ex.Message}");
            }

        }

    }
}
