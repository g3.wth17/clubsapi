﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class ClubService : IClubService
    {
        private IInnovationClubsRepository repository;
        public ClubService(IInnovationClubsRepository repository)
        {
            this.repository = repository;
        }
        public Club AddClub(Club club, int ownerId)
        {
            //Validar el id del owner
            club.AttendantId = ownerId;
            return repository.CreateClub(club);
        }

        public Club GetClub(int id, int ownerId)
        {
            //Revisar o validad el dueno, algo que se tiene que implementar
            var clubFound = repository.GetClub(id, ownerId);
            if (clubFound == null)
                throw new Exception("Club not created");
            return clubFound;
        }

        public Club GetClub(int id)
        {
            var clubFound = repository.GetClub(id);
            if (clubFound == null)
                throw new Exception("Club not created");
            return clubFound;
        }

        public IEnumerable<Club> GetClubs()
        {
            return repository.GetClubs().OrderBy(c => c.Id);   
        }
    }
}
