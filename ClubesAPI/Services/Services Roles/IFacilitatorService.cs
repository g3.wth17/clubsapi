﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IFacilitatorService
    {
        IEnumerable<Facilitator> GetFacilitators();
        Facilitator GetFacilitator(int id);
        Facilitator AddFacilitator(Facilitator facilitator);
    }
}
