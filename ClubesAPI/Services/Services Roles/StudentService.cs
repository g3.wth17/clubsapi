﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class StudentService : IStudentService
    {
        private IInnovationClubsRepository innovationClubsRepository;
        public StudentService(IInnovationClubsRepository innovationClubsRepository)
        {
            this.innovationClubsRepository = innovationClubsRepository;
        }
        public Student AddStudent(Student student)
        {
            return innovationClubsRepository.CreateStudent(student);
        }

        public Student GetStudent(int id)
        {
            var student = innovationClubsRepository.GetStudent(id);
            if (student==null)
            {
                throw new Exception("Student not found");
            }
            return student;
        }

        public IEnumerable<Student> GetStudents()
        {
            return innovationClubsRepository.GetStudents().OrderBy(x => x.Id);
        }
    }
}
