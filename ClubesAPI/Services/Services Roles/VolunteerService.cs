﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class VolunteerService : IVolunteerService
    {
        private IInnovationClubsRepository repository;
        public VolunteerService(IInnovationClubsRepository repository)
        {
            this.repository = repository;
        }
        public Volunteer AddVolunteer(Volunteer volunteer)
        {
            return repository.CreateVolunteer(volunteer);
        }

        public Volunteer GetVolunteer(int id)
        {
            var volunteerFound = repository.GetVolunteer(id);
            if (volunteerFound == null)
                throw new Exception("Volunteer not found");
            return volunteerFound;
        }

        public IEnumerable<Volunteer> GetVolunteers()
        {
            return repository.GetVolunteers().OrderBy(v => v.Id);
        }
    }
}
