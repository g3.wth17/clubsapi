﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class FacilitatorService : IFacilitatorService
    {
        private IInnovationClubsRepository repository;
        public FacilitatorService(IInnovationClubsRepository repository)
        {
            this.repository = repository;
        }
        public Facilitator AddFacilitator(Facilitator facilitator)
        {
            return repository.CreateFacilitator(facilitator);
        }

        public Facilitator GetFacilitator(int id)
        {
            var facilitatorFound = repository.GetFacilitator(id);
            if (facilitatorFound == null)
                throw new Exception("Facilitator not found");
            return facilitatorFound;
        }

        public IEnumerable<Facilitator> GetFacilitators()
        {
            return repository.GetFacilitators().OrderBy(f => f.Id);
        }
    }
}
