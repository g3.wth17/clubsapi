﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IMentorService 
    {
        IEnumerable<Mentor> GetMentors();
        Mentor GetMentor(int id);
        Mentor AddMentor(Mentor mentor);
    }
}
