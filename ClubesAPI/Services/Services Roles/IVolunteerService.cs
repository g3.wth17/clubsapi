﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IVolunteerService
    {
        IEnumerable<Volunteer> GetVolunteers();
        Volunteer GetVolunteer(int id);
        Volunteer AddVolunteer(Volunteer volunteer);
    }
}
