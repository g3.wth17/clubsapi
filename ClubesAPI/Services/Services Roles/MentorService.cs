﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class MentorService : IMentorService
    {
        private IInnovationClubsRepository innovationClubsRepository;
        public MentorService(IInnovationClubsRepository innovationClubsRepository)
        {
            this.innovationClubsRepository = innovationClubsRepository;
        }

        public Mentor AddMentor(Mentor mentor)
        {
            return innovationClubsRepository.CreateMentor(mentor);
        }

        public Mentor GetMentor(int id)
        {
            var mentor = innovationClubsRepository.GetMentor(id);
            if (mentor == null)
            {
                throw new Exception("Student not found");
            }
            return mentor;
        }

        public IEnumerable<Mentor> GetMentors()
        {
            return innovationClubsRepository.GetMentors().OrderBy(x => x.Id);
        }
    }
}
