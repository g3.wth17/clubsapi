﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public class AdminService : IAdminService
    {
        private  IInnovationClubsRepository innovationClubsRepository;
        public AdminService(IInnovationClubsRepository innovationClubsRepository)
        {
            this.innovationClubsRepository = innovationClubsRepository;
        }
        public Admin AddAdmin(Admin admin)
        {
            return innovationClubsRepository.CreateAdmin(admin);
        }

        public Admin GetAdmin(int id)
        {
            var admin = innovationClubsRepository.GetAdmin(id);
            if (admin == null)
            {
                throw new Exception("Student not found");
            }
            return admin;
        }

        public IEnumerable<Admin> GetAdmins()
        {
            return innovationClubsRepository.GetAdmins().OrderBy(x => x.Id);
        }
    }
}
