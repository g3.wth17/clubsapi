﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IAdminService
    {
        IEnumerable<Admin> GetAdmins();
        Admin GetAdmin(int id);
        Admin AddAdmin(Admin admin);
    }
}
