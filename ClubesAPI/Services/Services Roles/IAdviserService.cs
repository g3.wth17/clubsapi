﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IAdviserService
    {
        IEnumerable<Adviser> GetAdvisers();
        Adviser GetAdviser(int id);
        Adviser AddAdviser(Adviser adviser);
    }
}
