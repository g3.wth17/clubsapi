﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    
    public class AdviserService : IAdviserService
    {
        private IInnovationClubsRepository repository;
        public AdviserService(IInnovationClubsRepository repository)
        {
            this.repository = repository;   
        }
        public Adviser AddAdviser(Adviser adviser)
        {
            return repository.CreateAdviser(adviser);
        }

        public Adviser GetAdviser(int id)
        {
            var adviserFound = repository.GetAdviser(id);
            if (adviserFound == null)
                throw new Exception("Adviser not found");
            return adviserFound;
        }

        public IEnumerable<Adviser> GetAdvisers()
        {
            return repository.GetAdvisers().OrderBy(a => a.Id);
        }
    }
}
