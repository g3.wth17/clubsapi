﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services.Services_Facilitator_Tools
{
    public class ModuleService : IModuleService
    {
        private IInnovationClubsRepository innovationClubsRepository;
        public ModuleService(IInnovationClubsRepository innovationClubsRepository)
        {
            this.innovationClubsRepository = innovationClubsRepository;
        }
        public Module AddModule(int FacilitatorId, int CourseId, Module module)
        {
            if (ValidateFacilitatorAndCourse(FacilitatorId, CourseId))
            {
                module.IdFacilitator = FacilitatorId;
                module.IdCourse = CourseId;
                return innovationClubsRepository.CreateModule(module);
            }
            throw new Exception("There is a problem with the Facilitator Id or Course Id");
        }

        public Module GetModule(int FacilitatorId, int CourseId, int id)
        {
            if (ValidateFacilitatorAndCourse(FacilitatorId, CourseId))
            {
                var module = innovationClubsRepository.GetModule(id, CourseId,FacilitatorId);
                if (module == null)
                {
                    throw new Exception("module not found");
                }
                return module;
            }
            throw new Exception("There is a problem with the Facilitator Id or Course Id");
        }

        public IEnumerable<Module> GetModules(int FacilitatorId, int CourseId)
        {
            if (ValidateFacilitatorAndCourse(FacilitatorId, CourseId))
            {
                var ListModules = innovationClubsRepository.GetModules(FacilitatorId, CourseId);
                if (ListModules == null)
                {
                    throw new Exception("Modules not found");
                }
                return ListModules;
            }
            throw new Exception("There is a problem with the Facilitator Id or Course Id");
        }


        private bool ValidateFacilitatorAndCourse(int FacilitatId,int CourseId) 
        {
            var Course = innovationClubsRepository.GetCourse(FacilitatId,CourseId);
            if (Course != null)
            {
                return true;
            }
            return false;
        }
    }
}
