﻿using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services.Services_Facilitator_Tools
{
    public interface IModuleService
    {
        IEnumerable<Module> GetModules(int FacilitatorId,int CourseId);
        Module GetModule(int FacilitatorId, int CourseId, int id);
        Module AddModule(int FacilitatorId, int CourseId, Module module);
    }
}
