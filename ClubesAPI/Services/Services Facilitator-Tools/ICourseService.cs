﻿using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services.Services_Facilitator_Tools
{
    public interface ICourseService
    {
        IEnumerable<Course> GetCourses(int FacilitatorId);
        Course GetCourse(int FacilitatorId, int id);
        Course AddCourse(int FacilitatorId, Course course);
    
    }
}
