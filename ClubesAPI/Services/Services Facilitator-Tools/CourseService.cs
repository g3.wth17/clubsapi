﻿using ClubesAPI.Data.Repository;
using ClubesAPI.Models.Facilitator_Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services.Services_Facilitator_Tools
{
    public class CourseService : ICourseService
    {
        private IInnovationClubsRepository innovationClubsRepository;

        public CourseService(IInnovationClubsRepository innovationClubsRepository)
        {
            this.innovationClubsRepository = innovationClubsRepository;
        }
        public Course AddCourse(int FacilitatorId, Course course)
        {
            if (ValidateFacilitador(FacilitatorId))
            {
                course.IdFacilitator = FacilitatorId;
                return innovationClubsRepository.CreateCourse(course);
            }
            throw new Exception("Facilitator not found");
        }

        public Course GetCourse(int FacilitatorId, int id)
        {
            if (ValidateFacilitador(FacilitatorId))
            {
                var Course = innovationClubsRepository.GetCourse(FacilitatorId, id);
                if (Course==null)
                {
                    throw new Exception("Course not found");
                }
                return Course;
            }
            throw new Exception("Facilitator not found");
        }

        public IEnumerable<Course> GetCourses(int FacilitatorId)
        {
            if (ValidateFacilitador(FacilitatorId))
            {
                return innovationClubsRepository.GetCourses(FacilitatorId);
            }
            throw new Exception("Facilitator not found");
        }

        private bool ValidateFacilitador(int FacilitatorId) 
        {
            if (innovationClubsRepository.GetFacilitator(FacilitatorId) != null)
                return true;
            else
                return false;
         }  
    }
}
