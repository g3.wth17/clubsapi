﻿using ClubesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClubesAPI.Services
{
    public interface IClubService
    {
        IEnumerable<Club> GetClubs();
        Club GetClub(int id, int ownerId);
        Club GetClub(int id);
        Club AddClub(Club club, int ownerId);
    }
}
